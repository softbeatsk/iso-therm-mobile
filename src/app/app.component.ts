import { Component, OnInit } from '@angular/core';
import { AlertController, MenuController, Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Plugins } from '@capacitor/core';
import { GlobalService } from './services/global.service';
import { CommonService } from './services/common.service';
import { Router } from '@angular/router';
import { FingerprintService } from './services/fingerprint.service';
const { App } = Plugins;
const { Device } = Plugins;

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit {
  public selectedIndex = 0;
  public appPages = [
    {
      title: 'Dashboard',
      url: 'dashboard',
      icon: 'home-outline'
    },
    // {
    //   title: 'List Of Jobs',
    //   url: 'job-list',
    //   icon: 'list-sharp'
    // },
    {
      title: 'Jobs',
      url: 'tasks',
      icon: 'checkmark-circle-sharp'
    },
    // {
    //   title: 'Tasks Details',
    //   url: 'task-detail',
    //   icon: 'reader'
    // }
  ];
  appVersion: string;
  fingerPrintEnabled: boolean;
  isStart: boolean;
  fingerprintAvailable: boolean;
  user: any;
  style: any;

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private common: CommonService,
    public global: GlobalService,
    private router: Router,
    public alertController: AlertController,
    private menu: MenuController,
    private fingerprint: FingerprintService,
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(async () => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      const info = await Device.getInfo();
      this.appVersion = info.appVersion;
      console.log("info", info)
      this.fingerprint.checkIfAvailable((cb) => {
        this.fingerprintAvailable = cb
      })
      this.global.getStorage("fingerprintSession", (res) => {
        if (res && res != null) {
          this.fingerPrintEnabled = true;
          this.isStart = true
          this.global.fingerPrintData = res
        }
        else {
          this.isStart = false
        }
        console.log("res", res)
      });
      this.global.getStorage('token', (res) => {
        if (res && res != null) {
          this.global.loginData = res;
          this.global.tenantId = res.tenantId
          this.common.setInitialData((cb) => {
            if (cb != false) {
              this.global.setStorage('userInfo',cb)
              this.global.user = cb
            }

          });
          this.router.navigate(['/dashboard'])
        }
        else {
          this.router.navigate(['/login'])
        }
      })
    });
  }

  configureFingerPrint() {
    console.log("fingerP", this.fingerPrintEnabled)
    if (this.fingerPrintEnabled == true && this.isStart == false) {
      this.fingerprint.showFingerprintAuthDlg('appComponent', (cb) => {
        if (cb == false) {
          this.fingerPrintEnabled = false;
        }
      })
    }
    if (this.fingerPrintEnabled == false) {
      this.global.setStorage("fingerprintSession", null);
      this.global.fingerPrintData = null;
      this.global.presentToast("Fingerprint Login Disabled!");
    }
    this.isStart = false
  }

  goTo(page) {
    this.global.goToPage(page)
  }

  async logOut() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Confirm!',
      message: 'Are you sure, you want to Logout!!!! ',
      mode: "ios",
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
          }
        }, {
          text: 'Okay',
          handler: () => {
            this.global.clearStorage();
            this.global.presentLoading("Please wait....");
            setTimeout(() => {
              this.global.dismissLoading();
              this.global.loginData = null;
              this.global.setStorage('fingerprintSession', this.global.fingerPrintData)
              this.router.navigate(["/login"])
            }, 1000);
          }
        }
      ]
    });

    await alert.present();
  }

  ngOnInit() {

  }
}
