import { Injectable } from '@angular/core';
import { GlobalService } from './global.service';
import { Platform } from '@ionic/angular';
import { HttpService } from './http.service';
import { Router } from '@angular/router';
import * as lodash from 'lodash';
import { NotificationService } from './notification.service';

@Injectable({
  providedIn: 'root'
})
export class CommonService {

  constructor(
    private global: GlobalService,
    private platform: Platform,
    private http: HttpService,
    private router: Router,
    private notification: NotificationService,
  ) { }

  initializeApp(email, password) {
    let body = {
      "userNameOrEmailAddress": email,
      "password": password,
      "rememberClient": true
    }
    this.global.checkNetworkStatus().then((res) => {
      if (res == true) {
        this.global.presentLoading('Please wait...');
        this.http.GetUserByEmail(email, (callback) => {
          console.log("callback", callback);
          if (callback.success) {
            if (callback.success.data == "") {
              this.global.dismissLoading();
              this.global.presentToast("Data of this email does not exist!")
              return;
            }
            let userData = JSON.parse(callback.success.data);
            userData = userData.result;
            this.global.setStorage("tenantData", userData)
            this.global.userData = userData
            this.global.tenantId = userData.toString();
            this.http.authenticate(body, (cb) => {
              this.global.dismissLoading();
              if (cb.success) {
                let data = JSON.parse(cb.success.data);
                data = data.result
                data.email = email;
                data.password = password;
                data.tenantId = this.global.tenantId
                data.date = new Date();
                this.global.setStorage('token', data);
                this.global.loginData = data;
                if (this.global.fingerPrintData && this.global.fingerPrintData != null) {
                  if (this.global.fingerPrintData.email != this.global.loginData.email) {
                    this.global.setStorage("fingerprintSession", null);
                    this.global.fingerPrintData = null
                  }
                }
                this.notification.pushNotification()
                this.setInitialData((res) => {
                  this.global.user = res
                });
                this.router.navigate(["/dashboard"], { replaceUrl: true })
                this.global.presentToast('You have successfully authenticated!');
              }
              else {
                let error = JSON.parse(cb.error.error);
                console.log("err123", error.error.details)
                this.global.presentToast(error.error.details)
              }
            })
          }
          else {
            this.global.presentToast('Something Went Wrong! Please try again later')
          }
        })
      }
      else {
        this.global.presentAlert(this.global.networkHeader, this.global.networkBody)
      }
    })
  }

  setInitialData(cb) {
    console.log("fff", this.global.loginData)
    this.http.getUser((res) => {
      {
        // this.user = result;
        console.log("this.user", res)
        if (res.success) {
          let data = res.success.data;
          if (data != '') {
            data = JSON.parse(data);
            if (data.success == true) {
              cb(data.result)
            }
            else {
              cb(false)
            }
          }

        }
        else {

        }
        // this._userService.getRoles().subscribe((result2) => {
        //   this.roles = result2.items;
        //   this.setInitialRolesStatus();
        // });
      }
    }, this.global.loginData.userId)
  }

  getDataFromLocalStorage(cb, key, arg = '' || {}) {
    this.global.getStorage(key, (res) => {
      if (res && res != null) {
        cb(res);
      }
      this.goToCloud((callbackRes)=>{
        cb(callbackRes)
      },key, arg = '' || {});
    })
    // return new Promise((resolve,reject)=>{

    // })
  }

  goToCloud(callbackRes,key, arg = '' || {}) {
    this.global.checkNetworkStatus().then((status) => {
      if (status == true) {
        this.getDataFromCloud((callback) => {
          callbackRes(callback)
        }, key, arg)
      }
      else {

      }
    })
  }

  getDataFromCloud(cb, func, arg) {
    this.http[func](arg, (res) => {
      console.log(func, res)
      if (res.success) {
        let data = res.success.data;
        if (data != '') {
          data = JSON.parse(data);
          if (data.result.totalCount > 0) {
            data = data.result.items
            if (data.length > 0) {
              this.global.setStorage(func, data)
              cb(data)
            }
            else {
              cb(false)
            }
          }
          else {
            cb(false)
          }
        }

      }
      else {

      }
    })
  }

  GetAllUsersForDropDownFromStorage() {
    this.global.getStorage("GetAllUsersForDropDown", (res) => {

    })
  }
}
