import { Injectable } from '@angular/core';
import { FingerprintAIO } from '@ionic-native/fingerprint-aio/ngx';
import { GlobalService } from './global.service';

@Injectable({
  providedIn: 'root'
})
export class FingerprintService {

  constructor(
    private faio: FingerprintAIO,
    private global: GlobalService,
  ) { }

  public showFingerprintAuthDlg(page, cb) {
    this.faio.isAvailable().then((result: any) => {
      console.log(result)

      this.faio.show({
        cancelButtonTitle: 'Cancel',
        description: "Use your fingerprint to verify your identity",
        disableBackup: true,
        title: 'Verify Your Identity',
        fallbackButtonTitle: 'FB Back Button',
      })
        .then((result: any) => {
          console.log(result)
          if (page == 'appComponent') {
            this.global.setStorage('fingerprintSession', this.global.loginData);
            this.global.fingerPrintData = this.global.loginData
            this.global.presentToast("Fingerprint Login Enabled!")
          }
          else if (page == 'login') {
            console.log("login", this.global.fingerPrintData)
          }
          cb(true)
        })
        .catch((error: any) => {
          console.log(error);
          cb(false)
          // alert("Match not found!")
        });

    }).catch((error: any) => {
      console.log(error)
    });
  }

  checkIfAvailable(cb) {
    this.faio.isAvailable().then((result: any) => {
      cb(true)
      console.log(result)
    }).catch((error: any) => {
      cb(false)
      console.log(error)
    });
  }
}
