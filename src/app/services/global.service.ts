import { Injectable } from '@angular/core';
const { Network } = Plugins;
import { Plugins, NetworkStatus, PluginListenerHandle } from '@capacitor/core';
import { AlertController, LoadingController, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { Subject } from 'rxjs';
import * as moment from 'moment'

@Injectable({
  providedIn: 'root'
})
export class GlobalService {
  loginData: any;
  networkStatus: NetworkStatus;
  networkListener: PluginListenerHandle;
  isStatusChanged: boolean;
  loading: HTMLIonLoadingElement;
  pendingApprovalsData: any;
  GetApprovedRequests: any;
  GetRejectedRequests: any;
  userData: any;
  tenantId: any;
  config: any;
  networkBody: any = 'Please check your internet connection and try again.'
  networkHeader: any = 'Network Issue!!'
  openFilterMenu = new Subject<any>();
  filterSearch = new Subject<any>();
  categorySearch = new Subject<any>();
  assignSearch = new Subject<any>();
  fingerPrintData: any;
  employeesData = new Subject<any>();
  user: any;
  currentTask: any;

  constructor(
    public alertController: AlertController,
    public toastController: ToastController,
    public loadingController: LoadingController,
    private storage: Storage,
    private router: Router,
    private location: Location,
  ) {
    //this.storage.clear()
  }

  checkPermissions(app, arg, action, cb, from = '') {
    console.log("this.config", this.config)
    if (this.config && this.config['pages.' + app] && (this.config['pages.' + app + '.' + arg + '.' + action] == true || this.config['pages.' + app + '.' + arg + '.' + action] == 'true')) {
      cb(true)
    }
    else {
      if (from == '') {
        this.presentAlert("Permission", "You don't have permission")
      }
      cb(false)
    }
  }

  setDate(date) {
    if (date != null) {
      var date1 = new Date(date); // Or the date you'd like converted.
      var isoDateTime = new Date(date1.getTime() - (date1.getTimezoneOffset() * 60000)).toISOString();
      return moment(isoDateTime).format('MMM DD, YYYY')
    }
    else {
      return null;
    }
  }

  setTime(date) {
    if (date != null) {
      var date1 = new Date(date); // Or the date you'd like converted.
      var isoDateTime = new Date(date1.getTime() - (date1.getTimezoneOffset() * 60000)).toISOString();
      return moment(isoDateTime).format('hh:mm A')
    }
    else {
      return null;
    }
  }

  setObservable(variable, value) {
    console.log("aaa")
    this[variable].next(value);
  }

  getObservable(variable): Subject<any> {
    return this[variable];
  }

  goToPage(page) {
    console.log("page", page)
    // if (page == "attendance") {
    //   this.presentAlert("Permission", "You don't have permission")
    //   return
    //   console.log("page", page)
    //   if (!this.userData.employeeId) {
    //     this.presentAlert("Permission", "You don't have permission")
    //   }
    //   else {
    //     this.router.navigate([page])
    //   }
    //   return
    // }
    let method = '';
    let action = '';
    let app = '';
    if (page == 'approval-logs/no') {
      method = 'approvallogs';
      app = 'hr';
      action = 'view';
    }
    else if (page == 'approval-logs/request') {
      this.router.navigate([page])
      return
    }
    else if (page == 'attendance-report') {
      method = 'attendancereport'
      app = 'hr';
      action = 'view'
      if (!this.userData.employeeId || this.userData.employeeId == null) {
        this.presentAlert("Permissions!", "Only Employee Can view the attendance report.")
        return
      }
      console.log("aaaa", this.loginData, this.userData)
      this.router.navigate([page])
      return
    }
    else if (page == 'pay-slip') {
      method = 'payslip'
      app = 'hr';
      action = 'view'
      this.router.navigate([page])
      return
    }
    else if (page == 'reimbursement') {
      method = 'reimbursement'
      app = 'hr';
      action = 'view'
      this.router.navigate([page])
      return
    }
    else if (page == 'leave-request') {
      method = 'leaverequest'
      app = 'hr';
      action = 'view'
      this.router.navigate([page])
      return
    }
    else if (page == 'tasks') {
      method = 'task'
      app = 'taskmanagement'
      action = 'view'
      this.router.navigate([page])
      return
    }
    else if (page == 'salary-advance') {
      method = 'advancerequest'
      app = 'hr';
      action = 'view'
    }
    else if (page == 'loan-request') {
      method = 'loanrequest'
      app = 'hr';
      action = 'view'
    }
    else if (page == 'hiring-request') {
      method = 'jobrequest'
      app = 'hr';
      action = 'view'
    }
    else if (page == 'attendance') {
      method = 'markattendance'
      app = 'hr';
      action = 'view'
    }
    else {
      this.router.navigate([page])
      return
    }
    this.checkPermissions(app, method, action, (cb) => {
      console.log("permi", cb)
      if (cb == true) {
        this.router.navigate([page])
      }
    })

  }

  backPage() {
    this.location.back();
  }

  async presentToast(text) {
    const toast = await this.toastController.create({
      message: text,
      duration: 3000,
      position: "bottom",
    });
    toast.present();
  }

  async presentLoading(text) {
    this.loading = await this.loadingController.create({
      message: text,
      backdropDismiss: true,
      mode: "ios",
      spinner: "circular"
    });
    await this.loading.present();

    const { role, data } = await this.loading.onDidDismiss();
  }

  dismissLoading() {
    if (this.loadingController) {
      this.loadingController.dismiss();
    }

  }

  clearStorage() {
    this.storage.clear()
  }

  getStorage(key, cb) {
    this.storage.get(key).then((res) => {
      cb(res);
    })
  }

  setStorage(key, value) {
    this.storage.set(key, value);
  }

  async presentAlert(header, text) {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: header,
      message: text,
      buttons: ['OK'],
      mode: "ios"
    });

    await alert.present();
  }

  async checkNetworkStatus() {
    this.networkListener = Network.addListener('networkStatusChange', (status) => {
      console.log("Network status changed", status);
      this.networkStatus = status;
      if (this.networkStatus.connected == true && this.isStatusChanged == false) {
        this.isStatusChanged = true
        // this.checkTokenValidity((callback) => {
        //   if (callback == true) {
        //     this.uploadPendingOrders();
        //     this.uploadPendingUpdateOrders();
        //     this.DeletePendingOrders();
        //   }
        // })

      }
    });

    this.networkStatus = await Network.getStatus();
    if (this.networkStatus && this.networkStatus.connected == true) {
      return true;
    }
    else {
      return false
    }
  }
}
