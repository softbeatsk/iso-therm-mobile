import { Injectable } from '@angular/core';
import { HTTP } from '@ionic-native/http/ngx';
import { environment } from '../../environments/environment';
import { GlobalService } from './global.service';
import { AlertController } from '@ionic/angular';
import { FingerprintService } from './fingerprint.service';
import * as lodash from 'lodash'
import { HttpClient, HttpHeaders, HttpResponseBase } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HttpService {
  authenticateURL: string = "api/TokenAuth/Authenticate";
  GetAllJobsURL: string = "api/services/app/Job/GetAll";
  GetAllTasksURL: string = "api/services/app/JobTask/GetAll";
  GetAllTaskDetailURL: string = "api/services/app/TaskActivity/GetAll";
  CreateTaskActivityURL: string = "api/services/app/TaskActivity/Create";
  UpdateTaskActivityURL: string = "api/services/app/TaskActivity/Update";
  getUserURL: string = "api/services/app/User/Get";
  GetUserByEmailURL: string = "api/services/app/User/GetUserByEmail";
  GetTaskTypeURL: string = "api/services/app/TaskType/GetAll"


  constructor(
    private http: HTTP,
    private global: GlobalService,
    public alertController: AlertController,
    private fingerprint: FingerprintService,
    private httpClient: HttpClient,
  ) { }

  authenticate(body, cb) {
    console.log("body", body)
    this.http.setDataSerializer('json');
    this.http.post(environment.baseURL + this.authenticateURL, body, { "Abp.TenantId": this.global.tenantId }).then((res) => {
      cb({ success: res })
    }).catch((err) => {
      cb({ error: err })
    })
  }

  GetUserByEmail(email, cb) {
    this.http.get(environment.baseURL + this.GetUserByEmailURL + "?email=" + email, {}, {}).then((res) => {
      cb({ success: res })
    }).catch((err) => {
      cb({ error: err })
    })
  }

  getUser(cb, id) {
    this.checkTokenValidity((callback) => {
      if (callback == true) {
        let queryParam = '';
        if (id != '') {
          queryParam += '?Id=' + id
        }
        this.http.get(environment.baseURL + this.getUserURL + queryParam, {}, { "Abp.TenantId": this.global.tenantId, Authorization: "Bearer " + this.global.loginData.accessToken }).then((res) => {
          cb({ success: res })
        }).catch((err) => {
          cb({ error: err })
        })
      }
      else {

      }
    })
  }

  GetAllJobs(arg, cb) {
    this.checkTokenValidity((callback) => {
      if (callback == true) {
        let queryParam = '';
        if (arg != '') {
          queryParam += '?MaxResultCount=' + arg.MaxResultCount + '&SkipCount=' + arg.SkipCount
        }
        this.http.get(environment.baseURL + this.GetAllJobsURL + queryParam, {}, { "Abp.TenantId": this.global.tenantId, Authorization: "Bearer " + this.global.loginData.accessToken }).then((res) => {
          cb({ success: res })
        }).catch((err) => {
          cb({ error: err })
        })
      }
      else {

      }
    })
  }

  GetTaskTypes(arg, cb) {
    this.checkTokenValidity((callback) => {
      if (callback == true) {
        console.log("arg", arg)
        let queryParam = '';
        if (arg != '') {
          queryParam += '?TaskId=' + arg.TaskId
        }
        this.http.get(environment.baseURL + this.GetTaskTypeURL + queryParam, {}, { "Abp.TenantId": this.global.tenantId, Authorization: "Bearer " + this.global.loginData.accessToken }).then((res) => {
          cb({ success: res })
        }).catch((err) => {
          cb({ error: err })
        })
      }
      else {

      }
    })
  }

  GetAllTaskDetail(arg, cb) {
    this.checkTokenValidity((callback) => {
      if (callback == true) {
        console.log("arg", arg)
        let queryParam = '';
        if (arg != '') {
          if (arg.creatorUserName && arg.TaskId) {
            queryParam += '?TaskId=' + arg.TaskId + '&CreatorUserName=' + arg.creatorUserName;
          }
          else if (arg.creatorUserName) {
            queryParam += '?CreatorUserName=' + arg.creatorUserName;
          }
          else {
            queryParam += '?TaskId=' + arg.TaskId;
          }
        }
        this.http.get(environment.baseURL + this.GetAllTaskDetailURL + queryParam, {}, { "Abp.TenantId": this.global.tenantId, Authorization: "Bearer " + this.global.loginData.accessToken }).then((res) => {
          cb({ success: res })
        }).catch((err) => {
          cb({ error: err })
        })
      }
      else {

      }
    })
  }

  CreateTaskActivity(cb, body) {
    this.checkTokenValidity((callback) => {
      if (callback == true) {
        this.http.setDataSerializer('json');
        this.http.post(environment.baseURL + this.CreateTaskActivityURL, body, { "Abp.TenantId": this.global.tenantId, Authorization: "Bearer " + this.global.loginData.accessToken }).then((res) => {
          cb({ success: res })
        }).catch((err) => {
          cb({ error: err })
        })
      }
      else {

      }
    })
  }

  UpdateTaskActivity(cb, body) {
    this.checkTokenValidity((callback) => {
      if (callback == true) {
        this.http.setDataSerializer('json');
        this.http.put(environment.baseURL + this.UpdateTaskActivityURL, body, { "Abp.TenantId": this.global.tenantId, Authorization: "Bearer " + this.global.loginData.accessToken }).then((res) => {
          cb({ success: res })
        }).catch((err) => {
          cb({ error: err })
        })
      }
      else {

      }
    })
  }

  GetAllTasks(arg, cb) {
    console.log("arg", arg, this.global.tenantId, this.global.loginData)
    this.checkTokenValidity((callback) => {
      if (callback == true) {
        let queryParam = '';
        if (arg != '') {
          queryParam += '?MaxResultCount=' + arg.MaxResultCount + '&SkipCount=' + arg.SkipCount;
          if (arg.EmployeeId) {
            queryParam += '&EmployeeId=' + arg.EmployeeId;
          }
          if(arg.statusId){
            queryParam += "&StatusId=" + arg.statusId
          }
        }
        this.http.get(environment.baseURL + this.GetAllTasksURL + queryParam, {}, { "Abp.TenantId": this.global.tenantId, Authorization: "Bearer " + this.global.loginData.accessToken }).then((res) => {
          cb({ success: res })
        }).catch((err) => {
          cb({ error: err })
        })
      }
      else {

      }
    })
  }

  async checkTokenValidity(callback) {
    console.log("this.", this.global.loginData)
    let diff = (new Date().getTime() - new Date(this.global.loginData.date).getTime()) / 1000;
    if (diff > this.global.loginData.expireInSeconds) {
      this.global.dismissLoading();
      if (this.global.fingerPrintData && this.global.fingerPrintData != null) {
        this.fingerprint.showFingerprintAuthDlg('login', (cb) => {
          if (cb == true) {
            let handler = {
              email: this.global.fingerPrintData.email,
              password: this.global.fingerPrintData.password
            }
            this.login(handler, (res) => {
              callback(res)
            })
            this.alertController.dismiss();
          }
        })
      }
      const alert = await this.alertController.create({
        cssClass: 'my-custom-class',
        header: 'Please Login Again!',
        backdropDismiss: false,
        inputs: [
          {
            name: 'email',
            type: 'email',
            value: this.global.loginData.email
          },
          {
            name: 'password',
            type: 'password',
            value: this.global.loginData.password,
          },
        ],
        //message: 'Are you sure, you want to delete the order!!!! ',
        mode: "ios",
        buttons: [
          {
            text: 'Login',
            handler: (handler) => {
              this.global.presentLoading("Please wait...")
              if (handler.email != '' && handler.password != '') {
                this.login(handler, (res) => {
                  callback(res)
                })
              }
              else {
                this.global.presentToast("Please Enter Email and Password");
              }
            }
          }
        ]
      });
      await alert.present();
    }
    else {
      callback(true)
    }
  }

  login(handler, callback) {
    let body = {
      "userNameOrEmailAddress": handler.email,
      "password": handler.password,
      "rememberClient": true,
      "token": "string"
    }
    this.authenticate(body, (cb) => {
      this.global.dismissLoading();
      if (cb.success) {
        let data = JSON.parse(cb.success.data);
        data = data.result
        data.date = new Date()
        data.email = handler.email;
        data.password = handler.password;
        data.tenantId = this.global.tenantId
        this.global.setStorage('token', data);
        this.global.loginData = data;
        this.global.presentToast('You have successfully authenticated!');
        this.alertController.dismiss()
        callback(true);
      }
      else {
        callback(false);
        this.global.presentToast('Something Went Wrong! Please try again later')
      }
    })
  }

}
