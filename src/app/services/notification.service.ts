import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { OneSignal } from '@ionic-native/onesignal/ngx';
import { HttpService } from './http.service';
import { GlobalService } from './global.service';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  constructor(
    private oneSignal: OneSignal,
    private http: HttpService,
    private global: GlobalService
  ) {

  }

  pushNotification() {
    // this.oneSignal.startInit(environment.oneSignalAppId, environment.firebaseSenderId)
    this.oneSignal.getIds().then((result) => {
      console.log("re",result)
      let body = {
        userId: this.global.loginData.userId.toString(),
        deviceId: result.userId
      }
      // this.http.RegisterDeviecWithOneSignal(body, cb => {
      //   console.log("cbbb",cb)
      // })
      //this.sendNotification(["7af2881d-4e73-42c5-b83a-a263eecc1a5b"],"My Notification Header api","My Notification contents api")
    })

    this.oneSignal.handleNotificationReceived().subscribe((receive) => {
      console.log("receive", receive)
      // do something when notification is received
    });

    this.oneSignal.handleNotificationOpened().subscribe((open) => {
      console.log("open", open)
      // do something when a notification is opened
    });
    this.oneSignal.endInit();
  }

  sendNotification(playerIds, header, content) {
    let body = {
      // "app_id": environment.oneSignalAppId,
      "include_player_ids": playerIds,
      "headings": { "en": header },
      "contents": { "en": content },
      "data": { "task": "Data" }
    }
    // this.http.sendNotification(body, (cb) => {
    //   console.log("cb", cb);
    //   if (cb.success) {

    //   }
    //   else {

    //   }
    // })
  }
}
