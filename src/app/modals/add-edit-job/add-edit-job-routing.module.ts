import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddEditJobPage } from './add-edit-job.page';

const routes: Routes = [
  {
    path: '',
    component: AddEditJobPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AddEditJobPageRoutingModule {}
