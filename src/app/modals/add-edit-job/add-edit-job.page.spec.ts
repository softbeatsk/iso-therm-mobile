import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AddEditJobPage } from './add-edit-job.page';

describe('AddEditJobPage', () => {
  let component: AddEditJobPage;
  let fixture: ComponentFixture<AddEditJobPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddEditJobPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AddEditJobPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
