import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AddEditJobPageRoutingModule } from './add-edit-job-routing.module';

import { AddEditJobPage } from './add-edit-job.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AddEditJobPageRoutingModule
  ],
  declarations: [AddEditJobPage]
})
export class AddEditJobPageModule {}
