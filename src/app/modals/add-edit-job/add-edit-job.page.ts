import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-add-edit-job',
  templateUrl: './add-edit-job.page.html',
  styleUrls: ['./add-edit-job.page.scss'],
})
export class AddEditJobPage implements OnInit {

  constructor(
    private modalController: ModalController
    
  ) { }

  ngOnInit() {
  }

  async closeModal(text) {
    const onClosedData: string = text;
    await this.modalController.dismiss(onClosedData);
  }

}
