import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { IonicSelectableComponent } from 'ionic-selectable';
import { Observable, Subscription } from 'rxjs';
import { delay } from 'rxjs/operators';

@Component({
  selector: 'app-selection',
  templateUrl: './selection.component.html',
  styleUrls: ['./selection.component.scss'],
})
export class SelectionComponent implements OnInit {
  portsSubscription: Subscription;
  page: number;
  @ViewChild('portComponent') portComponent: IonicSelectableComponent;
  @Input() inputArray: any;
  @Input() name: any;
  @Input() id: any;
  @Input() text: any;
  @Output() getOutputs = new EventEmitter();
  input: any;
  constructor() { }

  ngOnInit() {
    console.log("aaaa", this.inputArray, this.name, this.id)
  }

  toggleItems() {
    this.portComponent.toggleItems(this.portComponent.itemsToConfirm.length ? false : true);

    // Confirm items and close Select page
    // without having the user to click Confirm button.
    // this.confirm();
  }

  clear() {
    this.portComponent.clear();
    this.portComponent.close();
  }

  confirm() {
    this.portComponent.confirm();
    this.portComponent.close();
  }

  getPorts(page?: number, size?: number, data?: any[]): any[] {
    let ports = [];
    ports = data;
    if (page && size) {
      ports = ports.slice((page - 1) * size, ((page - 1) * size) + size);
    }

    return ports;
  }

  getPortsAsync(page?: number, size?: number, data?: any, timeout = 1): Observable<any[]> {
    return new Observable<any[]>(observer => {
      observer.next(this.getPorts(page, size, data));
      observer.complete();
    }).pipe(delay(timeout));
  }

  filterPorts(ports, text: string) {
    return ports.filter(port => {
      return port.name.toLowerCase().indexOf(text) !== -1;
    });
  }

  searchItems(event: {
    component: IonicSelectableComponent,
    text: string
  }) {
    let getData = this.inputArray;
    console.log("here", event)
    let text = event.text.trim().toLowerCase();
    event.component.startSearch();

    // Close any running subscription.
    if (this.portsSubscription) {
      this.portsSubscription.unsubscribe();
    }

    if (!text) {
      // Close any running subscription.
      if (this.portsSubscription) {
        this.portsSubscription.unsubscribe();
      }
      event.component.items = this.getPorts(1, 20, getData);

      // Enable and start infinite scroll from the beginning.
      this.page = 2;
      event.component.endSearch();
      event.component.enableInfiniteScroll();
      return;
    }

    this.portsSubscription = this.getPortsAsync(null, null, getData).subscribe(ports => {

      // Subscription will be closed when unsubscribed manually.
      if (this.portsSubscription.closed) {
        return;
      }


      event.component.items = this.filterPorts(ports, text);
      event.component.endSearch();
    });
  }

  getMorePorts(event: {
    component: IonicSelectableComponent,
    text: string
  }, selectType) {
    let getData = this.inputArray;
    let pageNum = getData.length / 20;
    let text = (event.text || '').trim().toLowerCase();
    // There're no more ports - disable infinite scroll.
    if (this.page - 1 > pageNum) {
      event.component.disableInfiniteScroll();
      return;
    }

    this.getPortsAsync(this.page, 20, getData).subscribe(ports => {
      ports = event.component.items.concat(ports);

      if (text) {
        ports = this.filterPorts(ports, text);
      }

      event.component.items = ports;
      event.component.endInfiniteScroll();
      this.page++;
    });
  }

  getInputs($event) {
    console.log("$event", $event, this.input);
    this.getOutputs.emit(this.input)
  }

}
