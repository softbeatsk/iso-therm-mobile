import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IonicModule } from '@ionic/angular';

import { FormsModule } from '@angular/forms';
import { SelectionComponent } from './selection/selection.component';
import { IonicSelectableModule } from 'ionic-selectable';



@NgModule({
  declarations: [
    SelectionComponent,
  ],
  imports: [
    CommonModule,
    IonicModule,
    IonicSelectableModule,
    FormsModule
  ],
  exports: [
    SelectionComponent,
  ],
  entryComponents: [
    SelectionComponent,
  ]
})
export class ComponentsModule { }
