import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { CommonService } from 'src/app/services/common.service';
import { GlobalService } from 'src/app/services/global.service';
import { HttpService } from 'src/app/services/http.service';
import * as lodash from 'lodash';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage implements OnInit {
  items: any;

  constructor(
    private http: HttpService,
    public global: GlobalService,
    private router: Router,
    public alertController: AlertController,
    private common: CommonService,
  ) { }

  ngOnInit() {

  }

  ionViewDidEnter() {
    console.log("cccc123", this.global.user)
    this.global.getStorage("userInfo", (res) => {
      if (res && res != null) {
        this.global.user = res
        this.getTaskDetail();
      }
      else {
        this.common.setInitialData((res) => {
          this.global.user = res;
          this.global.setStorage('userInfo', res)
          this.getTaskDetail();
        })
      }
    })
  }

  getTaskDetail() {
    this.http.GetAllTaskDetail({ 'creatorUserName': this.global.user.fullName }, (cb) => {
      if (cb.success) {
        let data = cb.success.data;
        if (data != '') {
          data = JSON.parse(data);
          if (data.result.totalCount > 0) {
            this.items = data.result.items;
            this.items = lodash.uniqBy(this.items,'taskTypeId')
            console.log("fff", this.items)
            // if (this.setTime(items[0].stopTime) == "-") {

            // }
          }
          console.log("aaa", data)
        }
      }
      console.log("cccc", cb, this.global.user)
    })
  }

  timeConvert(n) {
    if (n) {
      var num = n;
      var hours = (num);
      var rhours = Math.floor(hours);
      var minutes = (hours - rhours) * 60;
      var rminutes = Math.round(minutes);
      return rhours + " Hrs " + rminutes + " Mins.";
    }

  }

  getTotalHours(start, stop) {
    stop = new Date(stop)
    if (stop.getFullYear() != 1) {
      var hours = Math.abs(stop.getTime() - new Date(start).getTime()) / 36e5;
      return hours
    }
  }


  goTo(page) {
    this.global.goToPage(page)
  }

  goToTask(page, task) {
    this.global.currentTask = task;
    this.router.navigate([page, { taskTypeId: task.id }])
  }

  async logOut() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Confirm!',
      message: 'Are you sure, you want to Logout!!!! ',
      mode: "ios",
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
          }
        }, {
          text: 'Okay',
          handler: () => {
            this.global.clearStorage();
            this.global.presentLoading("Please wait....");
            setTimeout(() => {
              this.global.dismissLoading();
              this.global.loginData = null;
              this.global.setStorage('fingerprintSession', this.global.fingerPrintData)
              this.router.navigate(["/login"])
            }, 1000);
          }
        }
      ]
    });

    await alert.present();
  }

  setTime(date) {
    let dateS = new Date(date);
    if (dateS.getFullYear() != 1) {
      return this.global.setTime(date)
    }
    else {
      return "-"
    }
  }

}
