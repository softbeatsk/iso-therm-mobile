import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CommonService } from 'src/app/services/common.service';
import { GlobalService } from 'src/app/services/global.service';

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.page.html',
  styleUrls: ['./tasks.page.scss'],
})
export class TasksPage implements OnInit {
  tasksArray: any = [];
  MaxResultCount: number = 10;
  SkipCount: number = 0;
  statusId: any;

  constructor(
    private common: CommonService,
    private global: GlobalService,
    private router: Router,
  ) { }

  ngOnInit() {
    // this.getAll((cb) => {
    //   if (cb != false) {
    //     this.tasksArray = cb;
    //     console.log("aaa", this.tasksArray)
    //   }
    //   else {
    //     this.tasksArray = [];
    //   }
    // })
  }

  ionViewWillEnter() {
    this.getAll((cb) => {
      if (cb != false) {
        this.tasksArray = cb;
        console.log("aaa", this.tasksArray)
      }
      else {
        this.tasksArray = [];
      }
    })
    // this.global.presentLoading('Please wait...')
    // this.common.getDataFromCloud((res) => {
    //   this.tasksArray = res;
    //   this.global.dismissLoading()
    // }, 'GetAllTasks', { MaxResultCount: this.MaxResultCount, SkipCount: this.SkipCount })
  }

  getAll(cb) {
    this.global.presentLoading('Please wait...')

    this.common.getDataFromCloud((res) => {
      this.global.dismissLoading()
      cb(res)
    }, 'GetAllTasks', { MaxResultCount: this.MaxResultCount, SkipCount: this.SkipCount, statusId: this.statusId ? this.statusId : '6' })
    // this.global.presentLoading('Please wait...')
    // this.common.getDataFromLocalStorage((res) => {
    //   console.log("GetAllTasks", res);
    //   this.global.dismissLoading()
    //   cb(res)
    // }, 'GetAllTasks', { MaxResultCount: this.MaxResultCount, SkipCount: this.SkipCount })
  }

  doRefresh(event) {
    console.log('Begin async operation');
    setTimeout(() => {
      console.log('Async operation has ended');
      this.getAll((cb) => {
        if (cb != false) {
          this.tasksArray = cb;
        }
        else {
          this.tasksArray = [];
        }
      })
      event.target.complete();
    }, 2000);
  }

  segmentChanged(event) {
    if (event.detail.value == "in-progress") {
      this.statusId = 1;
    }
    else {
      this.statusId = 2;
    }
    this.getAll((cb) => {
      if (cb != false) {
        this.tasksArray = cb;
      }
      else {
        this.tasksArray = [];
      }
    })
  }

  loadData(event) {
    this.SkipCount += 10;
    this.getAll((cb) => {
      if (cb != false) {
        this.tasksArray = this.tasksArray.concat(cb);
        event.target.complete();
      }
      else {
        event.target.disabled = true;
      }

    });
  }

  goTo(page, id) {
    this.router.navigate([page, { taskId: id }])
  }

  setDate(date) {
    return this.global.setDate(date)
  }

  setTime(date) {
    let time = date.split(".");
    return time[0] + ' Hrs ' + (time[1] ? time[1] + ' Mins' : '')
  }

}
