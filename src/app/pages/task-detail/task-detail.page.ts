import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { timer } from 'rxjs/internal/observable/timer';
import { CommonService } from 'src/app/services/common.service';
import { GlobalService } from 'src/app/services/global.service';
import { HttpService } from 'src/app/services/http.service';

@Component({
  selector: 'app-task-detail',
  templateUrl: './task-detail.page.html',
  styleUrls: ['./task-detail.page.scss'],
})
export class TaskDetailPage implements OnInit {
  MaxResultCount: number = 10;
  SkipCount: number = 0;
  tasksArray: any;
  taskId: any;
  showTimer: any = '00:00:00';
  taskDetail: any;
  taskTimer: Date;
  interval: any;
  isPause: boolean = false;
  currentUser: any;

  constructor(
    private common: CommonService,
    private global: GlobalService,
    private route: ActivatedRoute,
    private datePipe: DatePipe,
    private http: HttpService,
  ) { }

  ngOnInit() {

  }

  ionViewWillEnter() {
    console.log("this", this.global.currentTask)
    this.getAll((cb) => {
      if (cb == false) {
        this.tasksArray = [];
        this.taskDetail = this.global.currentTask;
        // this.setupTimer();
      }
      else {
        this.tasksArray = cb;
        this.taskDetail = this.global.currentTask;
        this.currentUser = this.tasksArray[0].creatorUserName
        this.setupTimer();
      }
    })
  }

  ionViewWillLeave() {
    this.taskTimer = null;
    this.global.currentTask = null
  }

  getAll(cb) {
    if (this.route.snapshot.params.taskTypeId) {
      this.taskId = this.route.snapshot.params.taskTypeId
      this.global.presentLoading('Please wait...')
      this.common.getDataFromCloud((res) => {
        console.log("GetAllTaskDetail", res);
        this.global.dismissLoading()
        cb(res)
      }, 'GetAllTaskDetail', { TaskId: this.taskId, MaxResultCount: this.MaxResultCount, SkipCount: this.SkipCount })
    }
  }

  doRefresh(event) {
    console.log('Begin async operation');
    setTimeout(() => {
      console.log('Async operation has ended');
      this.getAll((cb) => {
        console.log("bb", cb)
        if (cb == false) {
          this.tasksArray = [];
          this.taskDetail = this.global.currentTask;
          // this.setupTimer();
        }
        else {
          this.tasksArray = cb;
          this.taskDetail = this.global.currentTask;
          this.setupTimer();
        }

      })
      event.target.complete();
    }, 2000);
  }

  loadData(event) {
    this.SkipCount += 10;
    this.getAll((cb) => {
      console.log("fsdfsda", cb)
      if (cb != false) {
        this.tasksArray = this.tasksArray.concat(cb);
        event.target.complete();
      }
      else {
        event.target.disabled = true;
      }

    });
  }

  setupTimer() {
    if (this.setTime(this.tasksArray[0].stopTime) == "-" && !this.taskTimer && this.isPause == false) {
      let hours = this.getTotalHours(this.tasksArray[0].startTime, new Date().toISOString())
      this.taskTimer = new Date();
      this.taskTimer.setHours(0, 0, 0, 0)
      console.log("task", this.taskTimer)
      let seconds = this.convertHourstoSeconds(hours);
      this.taskTimer.setSeconds(this.taskTimer.getSeconds() + (seconds - 18000));
      this.startTimer();
      console.log("hours", hours, seconds)
    }
  }

  startTask() {
    this.currentUser = this.global.user.fullName
    let body = {
      taskTypeId: this.taskId,
      startTime: new Date().toISOString(),
      creatorUserName: this.currentUser
    }
    this.startTimer();
    this.http.CreateTaskActivity((cb) => {
      console.log("res", cb)
      if (cb.success) {
        let data = cb.success.data
        if (data != '') {
          let result = cb.success.result;
          this.getAll((cb) => {
            this.tasksArray = cb;
            this.taskDetail = this.global.currentTask;
            this.setupTimer();
          })
        }
      }
      else {

      }
    }, body)
  }

  startTimer() {
    if (!this.taskTimer || this.taskTimer == null) {
      this.taskTimer = new Date();
      this.taskTimer.setHours(0, 0, 0, 0)
    }
    this.isPause = false
    this.interval = timer(0, 1000).subscribe(() => {
      this.taskTimer.setSeconds(this.taskTimer.getSeconds() + 1);
      this.showTimer = this.datePipe.transform(this.taskTimer, 'HH:mm:ss')
    });
  }

  pauseTimer(arg = '') {
    this.interval.unsubscribe();
    this.isPause = true
    let body = {
      "taskTypeId": this.taskId,
      "taskType": null,
      "startTime": this.tasksArray[0].startTime,
      "stopTime": new Date().toISOString(),
      creatorUserName: this.currentUser,
      "id": this.tasksArray[0].id
    }
    console.log("body", body);
    this.http.UpdateTaskActivity((cb) => {
      console.log("cb", cb)
      if (cb.success) {
        let data = cb.success.data
        if (data != '') {
          data = JSON.parse(data);
          if (data.success == true) {
            console.log("result12", data)
            this.taskTimer = null;
            this.getAll((cb) => {
              this.tasksArray = cb;
              this.taskDetail = this.global.currentTask;
              this.setupTimer();
            })

            if (arg == 'complete') {
              // this.tasksService.update()
            }
          }

        }
      }
      else {

      }
    }, body)
    // this.time
  }

  convertHourstoSeconds(hours) {
    return Math.floor(hours * 60 * 60);
  }

  setDate(date) {
    return this.global.setDate(date)
  }

  setTime(date) {
    let dateS = new Date(date);
    if (dateS.getFullYear() != 1) {
      return this.global.setTime(date)
    }
    else {
      return "-"
    }
  }

  timeConvert(n) {
    if (n) {
      var num = n;
      var hours = (num);
      var rhours = Math.floor(hours);
      var minutes = (hours - rhours) * 60;
      var rminutes = Math.round(minutes);
      return rhours + " Hrs " + rminutes + " Mins.";
    }

  }

  getTotalHours(start, stop) {
    stop = new Date(stop)
    if (stop.getFullYear() != 1) {
      var hours = Math.abs(stop.getTime() - new Date(start).getTime()) / 36e5;
      return hours
    }
  }

}
