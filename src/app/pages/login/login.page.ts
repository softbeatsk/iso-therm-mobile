import { Component, OnInit } from '@angular/core';

import { Platform } from '@ionic/angular';

import { HttpService } from 'src/app/services/http.service';
import { GlobalService } from 'src/app/services/global.service';
import { Router } from '@angular/router';
import { CommonService } from 'src/app/services/common.service';
import { FingerprintService } from 'src/app/services/fingerprint.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  email: any //= 'adnan@iso-therm.com';
  password: any //= 'Adnan@123';

  constructor(
    public global: GlobalService,
    private platform: Platform,
    private http: HttpService,
    private router: Router,
    private common: CommonService,
    private fingerprint: FingerprintService
  ) {
  }

  ngOnInit() {
  }

  ionViewWillEnter() {
    if (this.global.loginData && this.global.loginData.accessToken) {
      this.router.navigate(["dashboard"]);
    }
  }

  login() {

    if (!this.email || this.email == '') {
      alert("Please enter email");
      return
    }
    if (!this.password || this.password == '') {
      alert("Please enter Password");
      return
    }
    this.common.initializeApp(this.email, this.password);
  }

  loginWithFingerPrint() {
    this.fingerprint.showFingerprintAuthDlg('login', (cb) => {
      if (cb == true) {
        this.common.initializeApp(this.global.fingerPrintData.email, this.global.fingerPrintData.password)
      }

    })
  }

}
