import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { AddEditJobPage } from 'src/app/modals/add-edit-job/add-edit-job.page';
import { CommonService } from 'src/app/services/common.service';
import { GlobalService } from 'src/app/services/global.service';

@Component({
  selector: 'app-job-list',
  templateUrl: './job-list.page.html',
  styleUrls: ['./job-list.page.scss'],
})
export class JobListPage implements OnInit {
  jobsArray: any = [];
  MaxResultCount: number = 10;
  SkipCount: number = 0;


  constructor(
    private modalController: ModalController,
    private common: CommonService,
    private global: GlobalService
  ) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.getAll((cb) => {
      if (cb != false) {
        this.jobsArray = cb;
      }
      else {
        this.jobsArray = [];
      }

    })
  }

  getAll(cb) {
    this.global.presentLoading('Please wait...')
    this.common.getDataFromLocalStorage((res) => {
      console.log("GetAllJobs1", res);
      this.global.dismissLoading()
      cb(res)
    }, 'GetAllJobs', { MaxResultCount: this.MaxResultCount, SkipCount: this.SkipCount })
  }

  doRefresh(event) {
    console.log('Begin async operation');
    setTimeout(() => {
      console.log('Async operation has ended');
      this.getAll((cb) => {
        if (cb != false) {
          this.jobsArray = cb;
        }
        else {
          this.jobsArray = [];
        }
      })
      event.target.complete();
    }, 2000);
  }

  loadData(event) {
    this.SkipCount += 10;
    this.getAll((cb) => {
      console.log("cb===", cb)
      if (cb != false) {
        this.jobsArray = this.jobsArray.concat(cb);
        event.target.complete();
      }
      else {
        event.target.disabled = true;
      }

    });
  }

  async addItem() {
    const modal = await this.modalController.create({
      component: AddEditJobPage,
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }

  setDate(date) {
    return this.global.setDate(date)
  }

  setTime(date) {
    return this.global.setTime(date)
  }
}
