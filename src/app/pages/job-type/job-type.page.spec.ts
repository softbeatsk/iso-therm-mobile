import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { JobTypePage } from './job-type.page';

describe('JobTypePage', () => {
  let component: JobTypePage;
  let fixture: ComponentFixture<JobTypePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JobTypePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(JobTypePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
