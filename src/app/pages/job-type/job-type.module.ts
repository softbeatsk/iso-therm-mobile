import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { JobTypePageRoutingModule } from './job-type-routing.module';

import { JobTypePage } from './job-type.page';
import { ComponentsModule } from 'src/app/components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    JobTypePageRoutingModule
  ],
  declarations: [JobTypePage]
})
export class JobTypePageModule { }
