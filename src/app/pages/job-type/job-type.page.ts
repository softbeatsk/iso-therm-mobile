import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CommonService } from 'src/app/services/common.service';
import { GlobalService } from 'src/app/services/global.service';

@Component({
  selector: 'app-job-type',
  templateUrl: './job-type.page.html',
  styleUrls: ['./job-type.page.scss'],
})
export class JobTypePage implements OnInit {
  tasksArray: any = [];
  MaxResultCount: number = 10;
  SkipCount: number = 0;
  taskId: any;

  constructor(
    private common: CommonService,
    private global: GlobalService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {

  }

  ionViewWillEnter() {
    this.getAll((cb) => {
      if (cb != false) {
        this.tasksArray = cb;
        console.log("aaa", this.tasksArray)
      }
      else {
        this.tasksArray = [];
      }
    })
  }

  getAll(cb) {
    if (this.route.snapshot.params.taskId) {
      this.taskId = this.route.snapshot.params.taskId
      this.global.presentLoading('Please wait...')
      this.common.getDataFromCloud((res) => {
        console.log("GetTaskTypes", res);
        this.global.dismissLoading()
        cb(res)
      }, 'GetTaskTypes', { MaxResultCount: this.MaxResultCount, SkipCount: this.SkipCount, TaskId: this.taskId, })
    }

  }

  doRefresh(event) {
    console.log('Begin async operation');
    setTimeout(() => {
      console.log('Async operation has ended');
      this.getAll((cb) => {
        if (cb != false) {
          this.tasksArray = cb;
        }
        else {
          this.tasksArray = [];
        }
      })
      event.target.complete();
    }, 2000);
  }

  loadData(event) {
    this.SkipCount += 10;
    this.getAll((cb) => {
      if (cb != false) {
        this.tasksArray = this.tasksArray.concat(cb);
        event.target.complete();
      }
      else {
        event.target.disabled = true;
      }

    });
  }

  goTo(page, task) {
    this.global.currentTask = task;
    this.router.navigate([page, { taskTypeId: task.id }])
  }

  setDate(date) {
    return this.global.setDate(date)
  }

  setTime(date) {
    let time = date.split(".");
    return time[0] + ' Hrs ' + (time[1] ? time[1] + ' Mins' : '')
  }

}
